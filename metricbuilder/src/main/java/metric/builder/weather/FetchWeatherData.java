package metric.builder.weather;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import org.json.JSONObject;

public class FetchWeatherData {
    
    private static final String OPEN_METEO_API = "https://api.open-meteo.com/v1/forecast?";
    private static final String PARAMS = "daily=temperature_2m_min,rain_sum,snowfall_sum,windspeed_10m_max&timezone=Europe%2FBerlin";
    //static String outputPath = "metricbuilder/src/main/java/metric/builder/weather/weather_data.json";
    static String outputPath = "/data/weather_data.json";

    public static JSONObject getWeather(double lat, double lon) throws IOException {

        // find out weather conditions

        // ice -> temperature min < 0 
        // rain -> rain_sum > 1 mm
        // snow -> snowfall_sum > 0.5 cm
        // wind -> windspeed > 22 km/h

        // construct the request URL
        String requestUrl = OPEN_METEO_API + "latitude=" + lat + "&longitude=" + lon + "&" + PARAMS;

        // send the request and parse the response
        URL url = new URL(requestUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        InputStream responseStream = connection.getInputStream();

        String responseString = new String(responseStream.readAllBytes(), StandardCharsets.UTF_8);

        JSONObject responseJson = new JSONObject(responseString);
        JSONObject dailyData = responseJson.optJSONObject("daily");

        //TODO is this still needed?
        // write weather data to a json file
        /*
        try {
            File file = new File(outputPath);
            //Make sure to delete the file if it already exists
            file.delete();
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(dailyData.toString());
            fileWriter.close();
         } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
        */
        return dailyData;

    }


}