package metric.builder.utility;

import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import de.fuberlin.navigator.protos.map_builder.Segment;
import de.fuberlin.navigator.protos.metric_builder.Metric;
import de.fuberlin.navigator.protos.metric_builder.Metrics;
import de.fuberlin.navigator.protos.metric_builder.MetricsPerDay;
import de.fuberlin.navigator.protos.metric_builder.WeatherCondition;
import metric.builder.model.weather.WeatherGrid;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class MetricApplier {

    private MetricApplier(){
    }

    public static MetricsPerDay apply(RoadNetwork roadNetwork, WeatherGrid grid, int dayAfterToday) {

        Collection<Segment> segmentColl = roadNetwork.getSegmentsMap().values();
        MetricsPerDay.Builder metricsBuilder = MetricsPerDay.newBuilder();
        metricsBuilder.setDayAfterToday(dayAfterToday);

        for (Segment segment : segmentColl) {

            Coordinates start = segment.getGeometry(0);
            Coordinates end = segment.getGeometry(segment.getGeometryCount() - 1);

            WeatherGrid.WeatherGridCell startCell = grid.parse(start);
            WeatherGrid.WeatherGridCell endCell = grid.parse(end);

            Metric.Builder metricBuilder = Metric.newBuilder()
                    .setSegmentId(segment.getId())
                    .setTravelSpeed(segment.getMaxSpeed());

            //We need to copy it otherwise we maybe about to override the tags field in a WeatherGridCell
            Set<WeatherCondition> tags = new HashSet<>(startCell.getTags(dayAfterToday));
            tags.addAll(endCell.getTags(dayAfterToday));
            for (WeatherCondition tag : tags) {
                metricBuilder.addAdditionalFactors(tag);
            }

            metricsBuilder.putMetricsPerDay(segment.getId(), metricBuilder.build());
        }

        return metricsBuilder.build();
    }
}
