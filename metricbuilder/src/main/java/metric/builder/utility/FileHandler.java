package metric.builder.utility;

import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import de.fuberlin.navigator.protos.metric_builder.Metrics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileHandler {

    private FileHandler() {
    }

    public static RoadNetwork loadFromFile(String inputPath) throws IOException {
        System.out.println("Start to load Road network.");
        FileInputStream input = new FileInputStream(inputPath);
        RoadNetwork roadNetwork = RoadNetwork.newBuilder().mergeFrom(input).build();
        input.close();
        System.out.println("Road network loaded.");

        return roadNetwork;
    }
    public static void saveToFile(Metrics metrics, String outputPath) throws IOException {
        System.out.println("Start to save metric to file.");
        FileOutputStream output = new FileOutputStream(outputPath);
        metrics.writeTo(output);
        output.close();
        System.out.println("Metric saved to file.");
    }
}
