package metric.builder.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private Config() {
    }

    private static Properties props = new Properties();

    public static String ROAD_NETWORK_INPUT_PATH = "./roadnetwork.proto";

    /**
     * Path under which the metric file should be saved
     */
    public static String METRICS_OUTPUT_PATH = "/../../../../metrics.proto";

    /**
     * Length and height of a weather grid cell measured in degrees
     */
    public static float WEATHER_GRID_CELL_DISTANCE = 0.5f;

    /**
     * Bounding box, default small box inside Cottbus
     */
    public static float BOUNDING_BOX_MIN_LAT = 51.765120241998865f;
    public static float BOUNDING_BOX_MIN_LON = 14.32669617537409f;
    public static float BOUNDING_BOX_MAX_LAT = 51.77116774623326f;
    public static float BOUNDING_BOX_MAX_LON = 14.330334220133722f;

    public static void load() {
        //The config is used for running the application in a Docker container
        String configFilePath = "/data/configuration/config.properties";

        try (FileInputStream is = new FileInputStream(configFilePath)){
            props.load(is);

            ROAD_NETWORK_INPUT_PATH = props.getProperty("METRIC_BUILDER_ROAD_NETWORK_INPUT_PATH", ROAD_NETWORK_INPUT_PATH);
            METRICS_OUTPUT_PATH = props.getProperty("METRIC_BUILDER_METRICS_OUTPUT_PATH", METRICS_OUTPUT_PATH);
            WEATHER_GRID_CELL_DISTANCE = Float.parseFloat(props.getProperty("METRIC_BUILDER_WEATHER_GRID_CELL_DISTANCE", String.valueOf(WEATHER_GRID_CELL_DISTANCE)));

            BOUNDING_BOX_MIN_LAT = Float.parseFloat(props.getProperty("BOUNDING_BOX_MIN_LAT", String.valueOf(BOUNDING_BOX_MIN_LAT)));
            BOUNDING_BOX_MIN_LON = Float.parseFloat(props.getProperty("BOUNDING_BOX_MIN_LON", String.valueOf(BOUNDING_BOX_MIN_LON)));
            BOUNDING_BOX_MAX_LAT = Float.parseFloat(props.getProperty("BOUNDING_BOX_MAX_LAT", String.valueOf(BOUNDING_BOX_MAX_LAT)));
            BOUNDING_BOX_MAX_LON = Float.parseFloat(props.getProperty("BOUNDING_BOX_MAX_LON", String.valueOf(BOUNDING_BOX_MAX_LON)));
            System.out.println("Config loaded.");
        } catch (FileNotFoundException e) {
            // Either way the Docker image was build wrong or this is not
            // run in a Docker environment
            System.out.println("No config file found. Using default settings!");
        } catch (IOException e) {
            // Something else went wrong, but we will
            // not let this escalate
            e.printStackTrace();
        }

    }
}
