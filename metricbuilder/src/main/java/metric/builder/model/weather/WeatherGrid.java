package metric.builder.model.weather;

import java.io.IOException;
import java.util.*;


import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.protos.metric_builder.WeatherCondition;
import metric.builder.utility.Config;
import metric.builder.weather.FetchWeatherData;
import org.json.JSONObject;

import static metric.builder.utility.Config.WEATHER_GRID_CELL_DISTANCE;

public class WeatherGrid {

    //Grid with each row of the outer list containing all Cells with the same lat, ordered by lat ASC
    //Inner lists are ordered by lon ASC
    private List<List<WeatherGridCell>> cells;

    public WeatherGrid(List<List<WeatherGridCell>> cells) {
        this.cells = cells;
    }

    /**
     * Performs search algorithm over the <code>cells</code> list.
     * Two phaes: first phase is performing a binary search over the outer list.
     * Second phase is performing a binary search on the inner list on the position
     * of the selected <code>lat</code> value of the first phase.
     * @param coordinates
     * @return
     */
    public WeatherGridCell parse(Coordinates coordinates) {
        int startIdx = 0, endIdx = cells.size() - 1, idx = cells.size() / 2;
        float lat = coordinates.getLat();
        float lon = coordinates.getLon();

        while (endIdx - startIdx > 1) {
            if (lat < cells.get(idx).get(0).getLat()
            && lat > cells.get(idx - 1).get(0).getLat()) {
                startIdx = idx - 1;
                endIdx = idx;
                break;
            } else if (lat > cells.get(idx).get(0).getLat()
                    && lat < cells.get(idx + 1).get(0).getLat()) {
                startIdx = idx;
                endIdx = idx + 1;
                break;
            } else if (lat < cells.get(idx).get(0).getLat()) {
                endIdx = idx - 1;
                idx = startIdx + ((endIdx - startIdx) / 2);
            } else if (lat > cells.get(idx).get(0).getLat()) {
                startIdx = idx + 1;
                idx = startIdx + ((endIdx - startIdx) / 2);
            } else {
                startIdx = idx;
                endIdx = idx;
                break;
            }
        }

        //startIdx marks smaller and endIdx larger element
        List<WeatherGridCell> col;
        if (Math.abs(cells.get(startIdx).get(0).getLat() - lat) < Math.abs(cells.get(endIdx).get(0).getLat() - lat)) {
            col = cells.get(startIdx);
        } else {
            col = cells.get(endIdx);
        }

        startIdx = 0;
        endIdx = col.size() - 1;
        idx = col.size() / 2;

        while (endIdx - startIdx > 1) {
            if (lon < col.get(idx).getLon()
                    && lon > col.get(idx - 1).getLon()) {
                startIdx = idx - 1;
                endIdx = idx;
                break;
            } else if (lon > col.get(idx).getLon()
                    && lon < col.get(idx + 1).getLon()) {
                startIdx = idx;
                endIdx = idx + 1;
                break;
            } else if (lon < col.get(idx).getLon()) {
                endIdx = idx - 1;
                idx = startIdx + ((endIdx - startIdx) / 2);
            } else if (lon > col.get(idx).getLon()) {
                startIdx = idx + 1;
                idx = startIdx + ((endIdx - startIdx) / 2);
            } else {
                startIdx = idx;
                endIdx = idx;
                break;
            }
        }

        //startIdx marks smaller and endIdx larger element
        WeatherGridCell cell;
        if (Math.abs(col.get(startIdx).getLon() - lon) < Math.abs(col.get(endIdx).getLon() - lon)) {
            cell = col.get(startIdx);
        } else {
            cell = col.get(endIdx);
        }

        return cell;
    }

    /**
     * Fetches weather data for the next 7 days in the specified bounding box.
     * @param minLat
     * @param minLon
     * @param maxLat
     * @param maxLon
     * @return WeatherGrid inside the bounding box with weather information for seven days for each cell
     * @throws IOException
     */
    public static WeatherGrid calculate(double minLat, double minLon, double maxLat, double maxLon) throws IOException {
        //Maybe fix wrong order
        minLat = (minLat < maxLat) ? minLat : maxLat;
        maxLat = (minLat < maxLat) ? maxLat : minLat;
        minLon = (minLon < maxLon) ? minLon : maxLon;
        maxLon = (minLon < maxLon) ? maxLon : minLon;

        System.out.println("Start to fetch weather data.");
        List<List<WeatherGridCell>> cells = new ArrayList<>();

        // ice -> temperature min < 0 
        // rain -> rain_sum > 1 mm
        // snow -> snowfall_sum > 0.5 cm
        // wind -> windspeed > 22 km/h

        for (double i = minLat; i <= maxLat; i = i + WEATHER_GRID_CELL_DISTANCE) {
            List<WeatherGridCell> col = new ArrayList<>();
            for (double j = minLon; j <= maxLon; j = j + WEATHER_GRID_CELL_DISTANCE) {

                JSONObject weatherData = FetchWeatherData.getWeather(i, j);
                WeatherGridCell cell = new WeatherGridCell(i, j);

                for (int dayAfterToday = 0; dayAfterToday < 7; dayAfterToday++) {
                    double temperature = weatherData.getJSONArray("temperature_2m_min").getDouble(dayAfterToday);
                    if (temperature < 0) {
                        cell.getTags(dayAfterToday).add(WeatherCondition.WEATHER_ICE);
                    }

                    double rain = weatherData.getJSONArray("rain_sum").getDouble(dayAfterToday);
                    if (rain > 1) {
                        cell.getTags(dayAfterToday).add(WeatherCondition.WEATHER_RAIN);
                    }

                    double snow = weatherData.getJSONArray("snowfall_sum").getDouble(dayAfterToday);
                    if (snow > 0.5) {
                        cell.getTags(dayAfterToday).add(WeatherCondition.WEATHER_SNOW);
                    }

                    double windspeed = weatherData.getJSONArray("windspeed_10m_max").getDouble(dayAfterToday);
                    if (windspeed > 22) {
                        cell.getTags(dayAfterToday).add(WeatherCondition.WEATHER_WIND);
                    }
                }

                col.add(cell);
            }

            cells.add(col);
        }

        System.out.println("Created weather grid.");
        return new WeatherGrid(cells);
    }

    public static class WeatherGridCell {
        private double lat;
        private double lon;

        private List<Set<WeatherCondition>> tagsPerDay;

        public WeatherGridCell(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
            this.tagsPerDay = new ArrayList<>(7);

            for (int i = 0; i < 7; i++) {
                this.tagsPerDay.add(new HashSet<>());
            }
        }


        public double getLat() {
            return lat;
        }

        public double getLon() {
            return lon;
        }

        public Set<WeatherCondition> getTags(int dayAfterToday) {
            return tagsPerDay.get(dayAfterToday);
        }
    }
}
    

