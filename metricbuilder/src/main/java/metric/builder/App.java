package metric.builder;

import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import de.fuberlin.navigator.protos.metric_builder.Metrics;
import metric.builder.model.weather.WeatherGrid;
import metric.builder.utility.Config;
import metric.builder.utility.FileHandler;
import metric.builder.utility.MetricApplier;

import java.io.IOException;

import static metric.builder.utility.Config.*;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) throws IOException {
        System.out.println( "Metric builder started!");

        Config.load();

        RoadNetwork roadNetwork = FileHandler.loadFromFile(ROAD_NETWORK_INPUT_PATH);

        WeatherGrid weatherGrid = WeatherGrid.calculate(BOUNDING_BOX_MIN_LAT, BOUNDING_BOX_MIN_LON, BOUNDING_BOX_MAX_LAT, BOUNDING_BOX_MAX_LON);

        Metrics.Builder metricsBuilder = Metrics.newBuilder();
        System.out.println("Start to create metric.");
        for (int i = 0; i < 7; i++) {
            metricsBuilder.putMetrics(i, MetricApplier.apply(roadNetwork, weatherGrid, i));
        }
        Metrics metrics = metricsBuilder.build();
        System.out.println("Metric created.");

        FileHandler.saveToFile(metrics, METRICS_OUTPUT_PATH);
        System.out.println("Finished!");
    }
}
